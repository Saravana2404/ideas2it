package com.demo.hospital;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * HospitalManagementApplication is the main class.
 *
 * @version 2.5.2 
 * @author	Saravana Kumar
 */
@SpringBootApplication(exclude = SecurityAutoConfiguration.class)
public class HospitalManagementApplication {
	
	/** main() method is the entry point of the program
	  *  JVM searches for main() method
	  */
	public static void main(String[] args) {
		
		SpringApplication.run(HospitalManagementApplication.class, args);
		
	}
	
	/**
	 * this method encrypts the data when called upon
	 * @return encrypted password
	 */
	@Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
