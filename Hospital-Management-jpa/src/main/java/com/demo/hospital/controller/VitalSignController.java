package com.demo.hospital.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.hospital.entity.Patient;
import com.demo.hospital.entity.User;
import com.demo.hospital.entity.VitalSign;
import com.demo.hospital.service.PatientService;
import com.demo.hospital.service.UserService;
import com.demo.hospital.service.VitalSignService;

/**
 * The VitalSignController class carries the rest api end points
 * of the vitalsign module
 * 
 * @author	Saravana Kumar
 */
@RestController
@RequestMapping("vital")
public class VitalSignController {
	
	/* Autowires the VitalSignService interface to do CRUD operation */
	@Autowired
	VitalSignService vitalService;
	
	/* Autowires the PatientService interface to get the patient object */
	@Autowired
	PatientService patientService;
	
	/**
	 * gets all the created vitals and returns it
	 * @return	list of vital signs of patients
	 */
	@GetMapping
	public List<VitalSign> getPatients() {
		return vitalService.listAllVital();
	}
	
	/**
	 * gets a particular vitalsign object using the id and returns it
	 * @param	the id of the vitalsign to retrive
	 * @return	the particular vitalsign of a patient
	 */
	@GetMapping("/{vitalId}")
	public ResponseEntity<VitalSign> getVital(@PathVariable int vitalId) {
		VitalSign vital=vitalService.getVital(vitalId);
		return new ResponseEntity<VitalSign>(vital, HttpStatus.OK);
	}
	
	/**
	 * creates a new vitalsign object with the help of post method
	 * @param	the vitalsign object that needs to be created
	 */
	@PostMapping
	public void createVital(@RequestBody 	VitalSign vitalSign) {	
		/* gets the patient object using the foreign key id of the patient */
		Patient patient=patientService.getPatient(vitalSign.getPatient().getId());
		vitalSign.setPatient(patient);
		vitalService.saveVital(vitalSign);
	}
	
	/**
	 * updates a particular vitalsign object using the id and returns it
	 * @param	the id of the vitalsign and the changes to the vitalsign object
	 * @return	the updated vitalsign object and the HTTP status code.
	 */
	@PutMapping(path="/{vitalId}")
	public ResponseEntity<VitalSign> updateVital(@PathVariable int vitalId, @RequestBody VitalSign vitalSign) {
		vitalSign.setId(vitalId);
		vitalService.saveVital(vitalSign);
		return new ResponseEntity<VitalSign>(HttpStatus.OK);
	}
	
	/**
	 * deletes a particular vitalsign object using the id 
	 * @param	the id of the vitalsign to be deleted
	 */
	@DeleteMapping(path="/{vitalId}")
	public void deletePatient(@PathVariable int vitalId) {
		vitalService.deleteVital(vitalId);
	}

}
