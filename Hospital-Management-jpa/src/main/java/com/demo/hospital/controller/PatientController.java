package com.demo.hospital.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.hospital.entity.Patient;
import com.demo.hospital.entity.User;
import com.demo.hospital.service.PatientService;
import com.demo.hospital.service.UserService;


/**
 * The PatientController class carries the rest api end points
 * of the patient module
 * 
 * @author	Saravana Kumar
 */
@RestController
@RequestMapping("patient")
public class PatientController {
	
	/* Autowires the PatientService interface to do CRUD operation */
	@Autowired
	PatientService patientService;
	
	/**
	 * gets all the created patients and returns it
	 * @return	list of patients
	 */
	@GetMapping
	public List<Patient> getPatients() {
		return patientService.listAllPatient();
	}
	
	/**
	 * gets a particular patient object using the id and returns it
	 * @param	the id of the patient to retrive
	 * @return	the particular patient
	 */
	@GetMapping("/{patientId}")
	public ResponseEntity<Patient> getPatient(@PathVariable long patientId) {
		
		Patient patient=patientService.getPatient(patientId);
		
		return new ResponseEntity<Patient>(patient, HttpStatus.OK);
	}
	
	/**
	 * creates a new patient object with the help of post method
	 * @param	the patient object that needs to be created
	 */
	@PostMapping
	public void createPatient(@RequestBody Patient patient) {
		patientService.savePatient(patient);
	}
	
	/**
	 * updates a particular patient object using the id and returns it
	 * @param	the id of the patient and the changes to the patient object
	 * @return	the updated patient object and the HTTP status code.
	 */
	@PutMapping(path="/{patientId}")
	public ResponseEntity<Patient> updatePatient(@PathVariable int patientId, @RequestBody Patient patient) {
		//patient.setP_id(patientId);
		patientService.savePatient(patient);
		return new ResponseEntity<Patient>(HttpStatus.OK);
	}
	
	/**
	 * deletes a particular patient object using the id 
	 * @param	the id of the patient to be deleted
	 */
	@DeleteMapping(path="/{patientId}")
	public void deletePatient(@PathVariable int patientId) {
		patientService.deletePatient(patientId);
	}

}
