package com.demo.hospital.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.security.access.prepost.PreAuthorize;
//import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.demo.hospital.entity.User;
import com.demo.hospital.service.UserService;

/**
 * The UserController class carries the rest api end points
 * of the user module
 * 
 * @author	Saravana Kumar
 */
@RestController
@RequestMapping("user")
public class UserController {
	
	/* Autowires the UserService interface to do CRUD operation */
	@Autowired
	UserService userService;
	
	/* Autowires the PasswordEncoder type to hash the password*/
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	/**
	 * gets all the created users and returns it
	 * @return	list of users
	 */
	@GetMapping
	public List<User> getUsers() {
		return userService.listAllUser();
	}
	
	/**
	 * gets a particular user object using the id and returns it
	 * @param	the id of the user to retrive
	 * @return	the particular user
	 */
	@GetMapping("/{userId}")
	public ResponseEntity<User> getUser(@PathVariable int userId) {
		User user=userService.getUser(userId);
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}
	
	/**
	 * creates a new user object with the help of post method
	 * @param	the user object that needs to be created
	 */
	@PostMapping
	public void createUser(@RequestBody User user) {
		/* encodes the password from the user object */
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		userService.saveUser(user);
	}
	
	/**
	 * updates a particular user object using the id and returns it
	 * @param	the id of the user and the changes to the user object
	 * @return	the updated user object and the HTTP status code.
	 */
	@PutMapping(path="/{userId}")
	public ResponseEntity<User> updateUser(@PathVariable int userId, @RequestBody User user) {
		user.setId(userId);
		userService.saveUser(user);
		return new ResponseEntity<User>(HttpStatus.OK);
	}
	
	/**
	 * deletes a particular user object using the id 
	 * @param	the id of the user to be deleted
	 */
	@DeleteMapping(path="/{userId}")
	public void deleteUser(@PathVariable int userId) {
		userService.deleteUser(userId);
	}
	

}
