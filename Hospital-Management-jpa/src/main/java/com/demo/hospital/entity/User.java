package com.demo.hospital.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This user class acts as the model class 
 * for the "user" table
 * 
 * @author	Saravana Kumar
 */
@Entity
@Table(name="user")
public class User {
	
	/* maps to the "id" column in the user table */
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private long id;
	
	/* maps to the "firstname" column in the user table */
	@Column(name="firstname")
	private String firstName;
	
	/* maps to the "lastname" column in the user table */
	@Column(name="lastname")
	private String lastName;
	
	/* maps to the "role" column in the user table */
	@Column(name="role")
	private String role;
	
	/* maps to the "email" column in the user table */
	@Column(name="email")
	private String email;
	
	/* maps to the "password" column in the user table */
	@Column(name="password")
	private String password;
	
	//getters and setters
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
