package com.demo.hospital.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * This patient class acts as the model class 
 * for the "patient" table
 * 
 * @author	Saravana Kumar
 */
@Entity
@Table(name="patient")
public class Patient {
	
	/* maps to the "id" column in the patient table */
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private long id;
	
	/* maps to the "address" column in the patient table */
	@Column(name="address")
	private String address;
	
	/* maps to the "last_visited" column in the patient table */
	@Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@Column(name="last_visited")
    private Date last_visited;
	
	/* maps to the "age" column in the patient table */
	@Column(name="age")
	private int age;
	
	/* maps to the "gender" column in the patient table */
	@Column(name="gender")
	private String gender;
	
	/* maps to the "createduser" column in the patient table */
	@Column(name="createduser")
	private long createdUser;	

//	public Patient() {
//	}
//	
//	public Patient(String address,Date last_visited,int age,String gender,long createdUser) {
//		this.address=address;
//		this.last_visited=last_visited;
//		this.age=age;
//		this.gender=gender;
//		this.createdUser=createdUser;
//	}
	
	//getters and setters
	public String getAddress() {
		return address;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getLast_visited() {
		return last_visited;
	}

	public void setLast_visited(Date last_visited) {
		this.last_visited = last_visited;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(long createdUser) {
		this.createdUser = createdUser;
	}

}
