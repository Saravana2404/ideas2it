package com.demo.hospital.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * This VitalSign class acts as the model class 
 * for the "vitalsign" table
 * 
 * @author	Saravana Kumar
 */
@Entity
@Table(name="vitalsign")
public class VitalSign {
	
	/* maps to the "id" column in the vitalsign table */
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	/* maps to the "temperature" column in the vitalsign table */
	@Column(name="temperature")
	private float temperature;
	
	/* maps to the "pulse" column in the vitalsign table */
	@Column(name="pulse")
	private int pulse;
	
	/* maps to the "blood_sugar" column in the vitalsign table */
	@Column(name="blood_sugar")
	private int blood_sugar;
	
	/* maps to the "blood_pressure" column in the vitalsign table */
	@Column(name="blood_pressure")
	private int blood_pressure;
	
	/**
	 * @OneToOne denotes one to one relationship between
	 * patient and vitalsign entites.
	 * @JoinColumn specifies the foreign key column 
	 * in the vitalsign table.
	 */
	@OneToOne
	@JoinColumn(name="patientid")
	private Patient patient;
	
	/* maps to the "createduser" column in the vitalsign table */
	@Column(name="createduser")
	private long createdUser;
	
	//getters and setters
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public float getTemperature() {
		return temperature;
	}

	public void setTemperature(float temperature) {
		this.temperature = temperature;
	}

	public int getPulse() {
		return pulse;
	}

	public void setPulse(int pulse) {
		this.pulse = pulse;
	}

	public int getBlood_sugar() {
		return blood_sugar;
	}

	public void setBlood_sugar(int blood_sugar) {
		this.blood_sugar = blood_sugar;
	}

	public int getBlood_pressure() {
		return blood_pressure;
	}

	public void setBlood_pressure(int blood_pressure) {
		this.blood_pressure = blood_pressure;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(long createdUser) {
		this.createdUser = createdUser;
	}

}
