package com.demo.hospital.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.hospital.entity.VitalSign;
import com.demo.hospital.repo.VitalSignRepository;

/**
 * The VitalSignServiceImpl class provides the implementation
 * of VitalSignService interface
 * 
 * @author	Saravana Kumar
 */
@Service
public class VitalSignServiceImpl implements VitalSignService{
	
	/*
	 * Autowire the VitalSignRepository 
	 * to retrieve and save data to database
	 */
	@Autowired
	private VitalSignRepository vitalRespository;
	
	/*
	 * implements the listAllVital() method of the User service interface
	 */
	public List<VitalSign> listAllVital(){
		return vitalRespository.findAll();
	}
	
	/*
	 * implements the saveVital() method of the User service interface
	 */
	public void saveVital(VitalSign vitalSign) {
		vitalRespository.save(vitalSign);
	}
	
	/*
	 * implements the getVital() method of the User service interface
	 */
	public VitalSign getVital(long id) {
		return vitalRespository.findById(id).get();
	}
	
	/*
	 * implements the deleteVital() method of the User service interface
	 */
	public void deleteVital(long id) {
		vitalRespository.deleteById(id);
	}

}
