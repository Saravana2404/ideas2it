package com.demo.hospital.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.hospital.entity.User;
import com.demo.hospital.repo.UserRepository;

/**
 * The UserServiceImpl class provides the implementation
 * of UserService interface
 * 
 * @author	Saravana Kumar
 */
@Service
public class UserServiceImpl implements UserService{
	
	/*
	 * Autowire the UserRepository 
	 * to retrieve and save data to database
	 */
	@Autowired
	private UserRepository userRespository;
	
	/*
	 * implements the listAllUser() method of the User service interface
	 */
	public List<User> listAllUser(){
		return userRespository.findAll();
	}
	
	/*
	 * implements the saveUser() method of the User service interface
	 */
	public void saveUser(User user) {
		userRespository.save(user);
	}
	
	/*
	 * implements the getUser() method of the User service interface
	 */
	public User getUser(long id) {
		return userRespository.findById(id).get();
	}
	
	/*
	 * implements the deleteUser() method of the User service interface
	 */
	public void deleteUser(long id) {
		userRespository.deleteById(id);
	}

}
