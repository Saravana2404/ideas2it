package com.demo.hospital.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.hospital.entity.Patient;
import com.demo.hospital.repo.PatientRepository;

/**
 * Patient service interface marks the CRUD operation
 * of the patient module
 * 
 * @author	Saravana Kumar
 */
public interface PatientService {
	
	/**
	 * method that lists all the patients.
	 */
	List<Patient> listAllPatient();
	
	/**
	 * method that creates a new patient.
	 * 
	 * @param the patient that will be saved.
	 */
	void savePatient(Patient patient);
	
	/**
	 * method that displays a particular patient.
	 * 
	 * @param the id of the particular patient.
	 */
	Patient getPatient(long id);
	
	/**
	 * method that deletes a patient.
	 * 
	 * @param the id of the patient which will be deleted.
	 */
	void deletePatient(long id);

}
