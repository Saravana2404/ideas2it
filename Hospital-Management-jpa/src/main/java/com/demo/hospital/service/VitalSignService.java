package com.demo.hospital.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.demo.hospital.entity.VitalSign;
import com.demo.hospital.repo.VitalSignRepository;

/**
 * VitalSign service interface marks the CRUD operation
 * of the patient module
 * 
 * @author	Saravana Kumar
 */
public interface VitalSignService {
	
	/**
	 * method that lists all the vital signs of a patient.
	 */
	List<VitalSign> listAllVital();
	
	/**
	 * method that creates a new vital sign of a patient.
	 * 
	 * @param the vital sign that will be saved.
	 */
	void saveVital(VitalSign vitalSign);
	
	/**
	 * method that displays a particular vital sign of a patient.
	 * 
	 * @param the id of the particular vitalsign.
	 */
	VitalSign getVital(long id);
	
	/**
	 * method that deletes a vital sign.
	 * 
	 * @param the id of the vital sign which will be deleted.
	 */
	void deleteVital(long id);
	
	

}
