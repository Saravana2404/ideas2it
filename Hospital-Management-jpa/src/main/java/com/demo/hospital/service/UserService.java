package com.demo.hospital.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.demo.hospital.entity.User;
import com.demo.hospital.repo.UserRepository;

/**
 * User service interface marks the CRUD operation
 * of the user module
 * 
 * @author	Saravana Kumar
 */
public interface UserService {
	
	/**
	 * method that lists all the users.
	 */
	List<User> listAllUser();
	
	/**
	 * method that creates a new users.
	 * 
	 * @param the user that will be saved.
	 */
	void saveUser(User user);
	
	/**
	 * method that displays a particular user.
	 * 
	 * @param the id of the particular user.
	 */
	User getUser(long id);
	
	/**
	 * method that deletes a user.
	 * 
	 * @param the id of the user which will be deleted.
	 */
	void deleteUser(long id);

}
