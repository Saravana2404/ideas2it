package com.demo.hospital.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.hospital.entity.Patient;
import com.demo.hospital.repo.PatientRepository;

/**
 * The PatientServiceImpl class provides the implementation
 * of PatientService interface
 * 
 * @author	Saravana Kumar
 */
@Service
public class PatientServiceImpl implements PatientService{
	
	/*
	 * Autowire the PatientRepository 
	 * to retrieve and save data to database
	 */
	@Autowired
	private PatientRepository patientRespository;
	
	/*
	 * implements the listAllPatient() method of the User service interface
	 */
	public List<Patient> listAllPatient(){
		return patientRespository.findAll();
	}
	
	/*
	 * implements the savePatient() method of the User service interface
	 */
	public void savePatient(Patient patient) {
		patientRespository.save(patient);
	}
	
	/*
	 * implements the getPatient() method of the User service interface
	 */
	public Patient getPatient(long id) {
		return patientRespository.findById(id).get();
	}
	
	/*
	 * implements the deletePatient() method of the User service interface
	 */
	public void deletePatient(long id) {
		patientRespository.deleteById(id);
	}

}
