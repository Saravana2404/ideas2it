package com.demo.hospital.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demo.hospital.entity.VitalSign;

/**
 * The VitalSignRepository interface is used to access VitalSign's data
 * from the database with the help of JpaRespository interface
 * 
 * @author	Saravana Kumar
 */
public interface VitalSignRepository extends JpaRepository<VitalSign, Long> {

}
