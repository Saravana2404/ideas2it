package com.demo.hospital.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demo.hospital.entity.Patient;

/**
 * The PatientRepository interface is used to access Patient's data
 * from the database with the help of JpaRespository interface
 * 
 * @author	Saravana Kumar
 */
public interface PatientRepository extends JpaRepository<Patient,Long> {
	

}
